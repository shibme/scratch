# Docker BuildX
[![Build Status](https://gitlab.com/shibme/docker-buildx/badges/main/pipeline.svg)](https://gitlab.com/shibme/docker-buildx/pipelines)

An image similar to [scratch](https://hub.docker.com/_/scratch) but can be used to mount and run anything locally